# Exercise3
Разработка аннотаций собственного фреймворка для тестирования

#Для запуска проекта скопируйте указанный ниже код
#Вставьте с ПОЛНОЙ заменой в файл tasks.json
#Расположение файла tasks.json: Explorer => первая папка ".theia" => tasks.json

# Запуск TestRunner c пакетами ( успешно )

{
    "tasks": [
        {
            "type": "che",
            "label": "TestRunner build and run",
            "command": "javac -d /projects/Lab3/MyTestFramework/bin -sourcepath /projects/Lab3/MyTestFramework/src /projects/Lab3/MyTestFramework/src/sample/TestRunner.java && java -classpath /projects/Lab3/MyTestFramework/bin sample.TestRunner",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Lab3/MyTestFramework/src",
                "component": "maven"
            }
        }
    ]
}
